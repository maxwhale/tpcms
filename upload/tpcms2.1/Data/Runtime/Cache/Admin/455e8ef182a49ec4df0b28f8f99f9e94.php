<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/tpcms2.0/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/tpcms2.0/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/tpcms2.0/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/tpcms2.0/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/tpcms2.0/",
	JS_ROOT: "/tpcms2.0/Core/Org/"
};
</script>
<script src="/tpcms2.0/Core/Org/wind.js"></script>
<script src="/tpcms2.0/Core/Org/jquery.js"></script>
</head>


    <body id="login-page">
        <div id="main-content">

            <!-- 主体 -->
            <div class="login-body">
                <div class="login-main pr">
                    <form action="<?php echo U('index');?>" method="post" class="login-form">
                        <h3 class="welcome"><i class="login-logo"></i>网站管理后台</h3>
                        <div id="itemBox" class="item-box">
                            <div class="item">
                                <i class="icon-login-user"></i>
                                <input type="text" name="username" placeholder="请填写用户名" autocomplete="off" />
                            </div>
                            <span class="placeholder_copy placeholder_un">请填写用户名</span>
                            <div class="item b0">
                                <i class="icon-login-pwd"></i>
                                <input type="password" name="password" placeholder="请填写密码" autocomplete="off" />
                            </div>
                            <span class="placeholder_copy placeholder_pwd">请填写密码</span>
                            <div class="item verifycode showcode"  style="display:none">
                                <i class="icon-login-verifycode"></i>
                                <input type="text" name="code" placeholder="请填写验证码" autocomplete="off">
                                <a class="reloadverify" title="换一张" href="javascript:void(0)">换一张？</a>
                            </div>
                            <span class="placeholder_copy placeholder_check">请填写验证码</span>
                            <div class="showcode">
                                <img class="verifyimg reloadverify" alt="点击切换" src="<?php echo U('verify',array('fontSize'=>25));?>">
                            </div>
                        </div>
                        <div class="login_btn_panel">
                            <button class="login-btn" type="submit">
                                <span class="in"><i class="icon-loading"></i>登 录 中 ...</span>
                                <span class="on">登 录</span>
                            </button>
                            <div class="check-tips"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <script type="text/javascript">
    	/* 登陆表单获取焦点变色 */
    	$(".login-form").on("focus", "input", function(){
            $(this).closest('.item').addClass('focus');
        }).on("blur","input",function(){
            $(this).closest('.item').removeClass('focus');
        });
        $('[name=password]').focus(function(){
             $.ajax({

                url:'<?php echo U("ajax_show_code");?>',
                dataType:'json',
                type:'post',
                success:function(res)
                {
                    if(res==1)
                        $('.showcode').show();
                    else
                        $('.showcode').hide();
                }
            })
        })
       




    	//表单提交
    	$(document)


	    	

	    	.ajaxStart(function(){
	    		$("button:submit").addClass("log-in").attr("disabled", true);
	    	})
	    	.ajaxStop(function(){
	    		$("button:submit").removeClass("log-in").attr("disabled", false);
	    	});

    	$("form").submit(function(){
    		var self = $(this);
    		$.post(self.attr("action"), self.serialize(), success, "json");
    		return false;

    		function success(data){
    			if(data.status){
    				window.location.href = data.url;
    			} else {
    				if(data.show_code==1)
    					$('.showcode').show();
    				self.find(".check-tips").text(data.info);
    				//刷新验证码
    				$(".reloadverify").click();
    			}
    		}
    	});

		$(function(){
			//初始化选中用户名输入框
			$("#itemBox").find("input[name=username]").focus();
			//刷新验证码
			var verifyimg = $(".verifyimg").attr("src");
            $(".reloadverify").click(function(){
                if( verifyimg.indexOf('?')>0){
                    $(".verifyimg").attr("src", verifyimg+'&random='+Math.random());
                }else{
                    $(".verifyimg").attr("src", verifyimg.replace(/\?.*$/,'')+'?'+Math.random());
                }
            });

            //placeholder兼容性
                //如果支持 
            function isPlaceholer(){
                var input = document.createElement('input');
                return "placeholder" in input;
            }
                //如果不支持
            if(!isPlaceholer()){
                $(".placeholder_copy").css({
                    display:'block'
                })
                $("#itemBox input").keydown(function(){
                    $(this).parents(".item").next(".placeholder_copy").css({
                        display:'none'
                    })                    
                })
                $("#itemBox input").blur(function(){
                    if($(this).val()==""){
                        $(this).parents(".item").next(".placeholder_copy").css({
                            display:'block'
                        })                      
                    }
                })
                
                
            }
		});
    </script>
    <style> 
    .showcode{
        display: none;
        }
     </style>
</body>
</html>