<?php if (!defined('THINK_PATH')) exit();?><div class="wrap J_check_wrap">
  <div class="nav">
    <ul class="cc">
         <li <?php if(!isset($_GET["verifystate"]) || $_GET["verifystate"] == 2): ?>class="current"<?php endif; ?>>
          <a href="<?php echo U('index',array('name'=>'Link','verifystate'=>2));?>">友情链接</a>
         </li>
        <li  <?php if(isset($_GET["verifystate"]) && $_GET["verifystate"] == 1): ?>class="current"<?php endif; ?>>
          <a href="<?php echo U('index',array('name'=>'Link','verifystate'=>1));?>">未审核链接</a>
        </li>
      </ul>
  </div>

   <div class="mb10">
    <a href="<?php echo U('add',array('name'=>'Link'));?>"  class="btn" title="添加链接"><span class="add"></span>添加链接</a>
       
      
  </div>
  <form class="J_ajaxForm" action="" method="post"> 
  <div class="table_list">
  <table width="100%" cellspacing="0" >
    <thead>
      <tr>
        <td width="10"><label><input type="checkbox" class="J_check_all" data-direction="x" data-checklist="J_check_x"></label></td>
        <td width="50" align="left">lid</td>
        <td width="80" align="left">排序</td>
        <td align="left">名称</td>
        <td align="left" width="80">类型</td>
        <td width="230" align="center">管理操作</td>
      </tr>
    </thead>
    <tbody>
    <?php if($data): if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr>
      	  <td><input type="checkbox" class="J_check" data-yid="J_check_y" data-xid="J_check_x" name="lid[<?php echo ($value["lid"]); ?>]" value="<?php echo ($value["lid"]); ?>"></td>
          <td ><?php echo ($value["lid"]); ?></td>
          <td ><input type="text" name="sort[<?php echo ($value["lid"]); ?>]" value="<?php echo ($value["sort"]); ?>" class="input" size='3'></td>
          <td ><a href="<?php echo ($value["url"]); ?>" target="_blank"><?php echo ($value["name"]); ?></a></td>
          <td ><?php if($value["logo"] == "" ): ?>文字<?php else: ?> 图片<?php endif; ?></td>
          <td align='center'>
          <a href="<?php echo ($value["url"]); ?>" target="_blank">访问</a> |
          <a href="<?php echo U('edit',array('name'=>'Link','lid'=>$value['lid'],'verfiystate'=>$value['verifystate']));?>">修改</a> |  <a class="J_ajax_del" href="<?php echo addons_url('Link://Link/del',array('lid'=>$value['lid'],'verifystate'=>$value['verifystate']));?>">删除</a></td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
      <?php else: ?>
       <tr>
        <td colspan="7">没有找到符合条件的记录</td>
       </tr><?php endif; ?>
     
     </tbody>

  </table>

   <div class="p10"><div class="pages"> <?php echo ($page); ?> </div> </div>
  </div>

     <div class="btn_wrap">
      <div class="btn_wrap_pd">
        <label class="mr20"><input type="checkbox" class="J_check_all" data-direction="y" data-checklist="J_check_y">全选</label>                
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo addons_url('Link://Link/sort');?>">排序</button>
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo addons_url('Link://Link/check');?>">审核</button>
        <button class="btn J_ajax_submit_btn" type="submit" data-action="<?php echo addons_url('Link://Link/cancel_check');?>">取消审核</button>
      
      </div>
    </div>
</form>
</div>