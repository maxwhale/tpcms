<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/dwz/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<?php echo ($custom_adminedit); ?>
<script type="text/javascript" src="/dwz/Core/Org/common.js"></script>
<script type="text/javascript" src="/dwz/Core/Org/content_addtop.js"></script>
</body>
</html>