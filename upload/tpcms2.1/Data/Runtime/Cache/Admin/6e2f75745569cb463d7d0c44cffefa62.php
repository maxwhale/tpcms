<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/dwz/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
  <div class="nav">
      <ul class="cc">
          <li class="current"><a href="javascript:;">插件列表</a></li>
      </ul>
    </div>
    <div class="h_a">使用说明</div>
  	<div>
  	  <div class="table_full">
	  <table width="100%" cellspacing="0" class='table_form'>
	  
		    <tbody>
		    <tr>
		    	<th width="100">插件名称：</th>
		    	<td><?php echo ($data["title"]); ?></td>
		    	
		    </tr>
		    <tr>
		    	<th width="100">插件描述：</th>
		    	<td><?php echo ($data["description"]); ?></td>
		    	
		    </tr>
		    <tr>
		    	<th width="100">作者：</th>
		    	<td><?php echo ($data["author"]); ?></td>
		    	
		    </tr>
		    <tr>
		    	<th width="100">详细说明：</th>
		    	<td><?php echo ($data["content"]); ?></td>
		    	
		    </tr>
		    <tr>
		    	<th>调用方法：</th>
		    	<td><input type="text" class="input select" style="width:50%" value="<?php echo ($data["call"]); ?>" readonly="readonly"></td>
		    </tr>

		    
		    </tbody>
		</table>
  		


  	</div>
  	<div class="">
      <div class="btn_wrap_pd">
      	<?php if(!$data["install"]): ?><button class="btn btn_submit mr10 " type="submit" onclick="location.href='<?php echo U('install',array('remark'=>$data['name']));?>'">安装</button>
        <?php else: ?>
        <button class="btn btn_submit  mr10 " onclick="location.href='<?php echo U('uninstall',array('remark'=>$data["name"]));?>'">卸载</button><?php endif; ?>
    	<button class="btn   mr10 " onclick="window.location.href='<?php echo U('lists');?>'">返回</button>
      </div>
    </div>
    </div>
</div>
<script type="text/javascript" src="/dwz/Core/Org/common.js"></script>
<script type="text/javascript">
    $('.select').click(function(){
        $(this).select();
    });
</script>
</body>
</html>