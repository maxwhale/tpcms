<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/tpcms2.0/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/tpcms2.0/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/tpcms2.0/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/tpcms2.0/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/tpcms2.0/",
	JS_ROOT: "/tpcms2.0/Core/Org/"
};
</script>
<script src="/tpcms2.0/Core/Org/wind.js"></script>
<script src="/tpcms2.0/Core/Org/jquery.js"></script>
</head>
<body>
<div class="wrap">
  <div id="home_toptip"></div>
  <h2 class="h_a">系统信息</h2>
  <div class="home_info">
    <ul>
      <?php $i=1; ?>
    	<?php if(is_array($info)): foreach($info as $key=>$value): ?><li <?php if($i%2 == 0): ?>style='background:#fff;font-weight:800'<?php endif; ?>>
	     		<em><?php echo ($key); ?></em>
	     		<span ><?php echo ($value); ?></span>
	     	</li>
       <?php $i++; endforeach; endif; ?>
     </ul>
  </div>
  <h2 class="h_a">开发团队</h2>
  <div class="home_info" id="home_devteam">
    <ul>
      <li>
        <em>版权所有</em> 
        <span><a href="http://www.djie.net" target="_blank">http://www.djie.net/</a></span> 
      </li>
      <li style='background:#fff;font-weight:800'>
        <em>负责人</em> 
        <span>点击未来</span> 
      </li>
     
    </ul>
  </div>
</div>
<script type="text/javascript" src="/tpcms2.0/Core/Org/common.js"></script>
<script type="text/javascript" src="/tpcms2.0/Core/Org/artDialog/artDialog.js"></script>
<script>
$("#btn_submit").click(function(){
	$("#tips_success").fadeTo(500,1);
});
artDialog.notice = function (options) {
    var opt = options || {},
        api, aConfig, hide, wrap, top,
        duration = 800;
        
    var config = {
        id: 'Notice',
        left: '100%',
        top: '100%',
        fixed: true,
        drag: false,
        resize: false,
        follow: null,
        lock: false,
        init: function(here){
            api = this;
            aConfig = api.config;
            wrap = api.DOM.wrap;
            top = parseInt(wrap[0].style.top);
            hide = top + wrap[0].offsetHeight;
            
            wrap.css('top', hide + 'px')
                .animate({top: top + 'px'}, duration, function () {
                    opt.init && opt.init.call(api, here);
                });
        },
        close: function(here){
            wrap.animate({top: hide + 'px'}, duration, function () {
                opt.close && opt.close.call(this, here);
                aConfig.close = $.noop;
                api.close();
            });
            
            return false;
        }
    };	
    
    for (var i in opt) {
        if (config[i] === undefined) config[i] = opt[i];
    };
    
    return artDialog(config);
};

</script>
</body>
</html>