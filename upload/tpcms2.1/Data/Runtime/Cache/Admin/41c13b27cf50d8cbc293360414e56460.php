<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/dwz/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<style>
.design_page li {
	height: 218px;
}
</style>
<div class="wrap J_check_wrap">
  <div class="nav">
    <ul class="cc">
        <li class="current"><a href="javascript:;">主题管理</a></li>
      </ul>
</div>
  <div class="mb10 cc">共 <span class="org"><?php echo ($count); ?></span> 套模板</div>
  <div class="design_page">
    <ul class="cc">
      <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><li>
          <div class="img">
            <a title="点击进行模板主题切换" href="<?php echo U('set_templates',array('filename'=>$value['filename']));?>">
              <img src="<?php echo ($value["image"]); ?>" width="210" height="140" lt="Default"></a></div>
          <div class="title" title="<?php echo ($value["name"]); ?>"><?php echo ($value["name"]); ?></div>
          <div class="ft"> 
           <?php if($value["current"]): ?><span class="org"><i>已使用</i></span>
           <?php else: ?>
             <span class="blue"><i>&nbsp;&nbsp;未使用</i></span><?php endif; ?>

          </div>
        </li><?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
  </div>
</div>
</body>
</html>