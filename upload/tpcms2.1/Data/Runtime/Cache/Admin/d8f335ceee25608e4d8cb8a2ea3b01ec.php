<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/tpcms2.0/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/tpcms2.0/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/tpcms2.0/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/tpcms2.0/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/tpcms2.0/",
	JS_ROOT: "/tpcms2.0/Core/Org/"
};
</script>
<script src="/tpcms2.0/Core/Org/wind.js"></script>
<script src="/tpcms2.0/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<style>
.pop_nav{
	padding: 0px;
}
.pop_nav ul{
	border-bottom:1px solid #266AAE;
	padding:0 5px;
	height:25px;
	clear:both;
}
.pop_nav ul li.current a{
	border:1px solid #266AAE;
	border-bottom:0 none;
	color:#333;
	font-weight:700;
	background:#F3F3F3;
	position:relative;
	border-radius:2px;
	margin-bottom:-1px;
}

</style>
<div class="wrap J_check_wrap">
  <div class="nav">
    <ul class="cc">
      <?php if($cat["cat_type"] == 4): ?><li  class="current"><a href="javascirpt:;"><?php echo ($cat["cname"]); ?>添加文档</a></li>
      <?php else: ?>
  		<li ><a href="<?php echo U('Article/index',array('category_cid'=>$_GET["category_cid"],'verifystate'=>2));?>"><?php echo ($cat["cname"]); ?>列表</a></li>
      <li class="current"><a href="javascrip:;">添加文档</a></li><?php endif; ?>
		
      </ul>
	</div>
  <div class="pop_nav">
    <ul class="J_tabs_nav">
      <li class="current"><a href="javascript:;">基本属性</a></li>
      <li class=""><a href="javascript:;">图集</a></li>
      <li class=""><a href="javascript:;">其他设置</a></li>
    </ul>
  </div>
  <form class="J_ajaxForms" name="myform" id="myform" action="<?php echo U('Article/add');?>" method="post">
    <div class="J_tabs_contents">
      <div>
        <div class="h_a">基本属性</div>
        <div class="table_full">
          <table width="100%" class="table_form ">
            <tr>
              <th  width="160">所属栏目：</th>
              <td>
              	<?php echo ($cat["cname"]); ?>
              	<input type="hidden" name="category_cid" id="category_cid" value="<?php echo ($_GET['category_cid']); ?>">
              </td>
            </tr>
             
           
           
            
            <tr>
              <th>标题：</th>
              <td><input type="text" name="article_title" id="article_title" class="input" value="" style='width:50%' ></td>
            </tr>
            <?php if(C("cfg_language_en")): ?><tr>
                <th>(英)标题：</th>
                <td><input type="text" name="article_title_en" id="article_title_en" class="input" value="" style='width:50%' ></td>
              </tr><?php endif; ?>

            <tr>
              <th ><strong>META Keywords(关键词)</strong><br/>
                关键字中间用半角逗号隔开</th>
              <td><textarea name='keywords' id='keywords' style="width:90%;height:80px"></textarea></td>
            </tr>
            <?php if(C("cfg_language_en")): ?><tr>
              <th >(英)<strong>META Keywords(关键词)</strong><br/>
                关键字中间用半角逗号隔开</th>
              <td><textarea name='keywords_en' id='keywords_en' style="width:90%;height:80px"></textarea></td>
            </tr><?php endif; ?>
            <tr>
              <th ><strong>META Description(描述)</strong><br/>
                针对搜索引擎设置的网页描述</th>
              <td><textarea name='description' id='description' style="width:90%;height:80px"></textarea></td>
            </tr>
            <?php if(C("cfg_language_en")): ?><tr>
              <th >(英)<strong>META Description(描述)</strong><br/>
                针对搜索引擎设置的网页描述</th>
              <td><textarea name='description_en' id='description_en' style="width:90%;height:80px"></textarea></td>
            </tr><?php endif; ?>
            <tr>
              <th>显示排序：</th>
              <td><input type="text" name="sort" id="sort" class="input" value="100"></td>
            </tr>
            <tr>
              <th>浏览次数：</th>
              <td><input type="text" name="click" id="click" class="input" value="900"></td>
            </tr>
            <tr>
              <th>发布时间：</th>
              <td><input type="text" name="addtime" id="addtime" class="input J_date" value="<?php echo format_date(time(),0);?>"></td>
            </tr>
            <tr>
              <th>文档属性：</th>
              <td>
                <?php if(is_array(C("flag"))): foreach(C("flag") as $key=>$v): ?><label><input type="checkbox" value="<?php echo ($v); ?>" name="flag[]"><?php echo ($v); ?></label>&nbsp;<?php endforeach; endif; ?>

              </td>
            </tr>
            <tr>
              <th>状态：</th>
              <td>
                <label><input type="checkbox" value="1" name="verifystate">未审核</label>&nbsp;
                <label><input type="checkbox" value="2" name="verifystate" checked="checked">通过</label>&nbsp;
               

              </td>
            </tr>
           
            <tr>
              <th>缩略图：</th>
              <td>
              <input type="text" name="pic" id="pic" value="" size="50" class="input"  ondblclick="image_priview(this.value);"/> 
              <input type="button" class="button" onclick="javascript:flashupload('image_images', '附件上传','pic',submit_images,'<?php echo (CONTROLLER_NAME); ?>',1,1,'<?php echo (C("cfg_image")); ?>')" value="上传图片" /><span class="gray"> 双击可以查看图片！</span>
              </td>
            </tr>
            <tr>
              <th>文件：</th>
              <td>
                <input type="text" name="file" id="file" value="" size="50" class="input"  ondblclick="image_priview(this.value);"/> 
                <input type="button" class="button" onclick="javascript:flashupload('image_images', '附件上传','file',submit_attachment,'<?php echo (CONTROLLER_NAME); ?>',1,0,'<?php echo (C("cfg_file")); ?>')" value="上传文件" />
              </td>
            </tr>

          
            <?php if(is_array($extForm)): $i = 0; $__LIST__ = $extForm;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr>
	              <th><?php echo ($value["title"]); ?>：</th>
	              <td><?php echo ($value["html"]); ?></td>
	            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
            <?php if($attrForm): ?><div class="h_a">筛选</div>
            <table width="100%" class="table_form ">
            <?php if(is_array($attrForm)): $i = 0; $__LIST__ = $attrForm;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr>
	              <th width="160"><?php echo ($value["title"]); ?>：</th>
	              <td><?php echo ($value["html"]); ?></td>
	            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
             </table><?php endif; ?>
        </div>
      </div>


      <div style="display:none">
        <div class="h_a">图集</div>
        <div class="table_full">
          <table width="100%" class="table_form ">
            
            <tr rel='0'>
              <th width="30" ><span class="hand plus_pics">[+]</span></th>
              <td>
              		<input type="text" name="pics[]" id="pics0" value="" size="50" class="input"  ondblclick="image_priview(this.value);"/> 
              		<input type="button" class="button" onclick="javascript:flashupload('image_images', '附件上传','pics0',submit_images,'<?php echo (CONTROLLER_NAME); ?>',1,1,'<?php echo (C("cfg_image")); ?>')" value="上传图片" /><span class="gray"> 双击可以查看图片！</span>
                  <input type="text" name="psort[]"  value="100" class="input" style="width:50px">
              </td>
            </tr>
            
          </table>
        </div>
      </div> 
   
      <div style="display:none">
        <div class="h_a">其他设置</div>
         <div class="table_full">
         <table width="100%" class="table_form ">
           <tr>
             <th width="160">自定义文件：</th>
             <td><input type="text" name="file_url" id="file_url" class="input" value="" style="width:60%"></td>
           </tr>
           <tr>
             <th>跳转地址：</th>
             <td><input type="text" name="jump_url" id="jump_url" class="input" value="" style="width:60%"></td>
           </tr>
         </table>
        </div>
      </div>

    </div>
 
    <div class="btn_wrap">
      <div class="btn_wrap_pd">
         <input type="hidden" name="isattr" value="0" />
        <button class="btn btn_submit mr10 " type="submit">提交</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript" src="/tpcms2.0/Core/Org/common.js"></script>
<script type="text/javascript" src="/tpcms2.0/Core/Org/content_addtop.js"></script>
<script type="text/javascript">
 var PUBLIC = '/tpcms2.0/Core/Org';
$(function(){
	
    Wind.use('validate', 'ajaxForm', 'artDialog', function () {
        var form = $('form.J_ajaxForms');
        //ie处理placeholder提交问题
        if ($.browser.msie) {
            form.find('[placeholder]').each(function () {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            });
        }
        //表单验证开始
        form.validate({
			//是否在获取焦点时验证
			onfocusout:false,
			//是否在敲击键盘时验证
			onkeyup:false,
			//当鼠标掉级时验证
			onclick: false,
            //验证错误
            showErrors: function (errorMap, errorArr) {
				//errorMap {'name':'错误信息'}
				//errorArr [{'message':'错误信息',element:({})}]
				try{
					$(errorArr[0].element).focus();
					art.dialog({
						id:'error',
						icon: 'error',
						lock: true,
						fixed: true,
						background:"#CCCCCC",
						opacity:0,
						content: errorArr[0].message,
						cancelVal: '确定',
						cancel: function(){
							$(errorArr[0].element).focus();
						}
					});
				}catch(err){
				}
            },
            
            <?php echo ($validate); ?>,
    
            //给未通过验证的元素加效果,闪烁等
            highlight: false,
            //是否在获取焦点时验证
            onfocusout: false,
            //验证通过，提交表单
            submitHandler: function (forms) {
                $(forms).ajaxSubmit({
                    url: form.attr('action'), //按钮上是否自定义提交地址(多按钮情况)
                    dataType: 'json',
                    beforeSubmit: function (arr, $form, options) {
                        
                    },
                    success: function (data, statusText, xhr, $form) {
                        if(data.status)
                        {
                          resultTip({error:0,msg:data.info});
                          setInterval(function()
                            {
                              window.location.href=data.url;
                            },1000);
                        }
                        else
                        {
                        	isalert(data.info);
                        }
                    }
                });
            }
        });
    });
});

$(function(){

	$('.plus_pics').click(function(){
		var obj = $(this).parents('tr').eq(0);
		var index = obj.parent().find('tr:last').attr('rel');
		index++;
		var html = '<tr rel="'+index+'">\
					<th ><span class="hand min_pics" >[-]</span></th>\
					<td><input type="text" name="pics[]" id="pics'+index+'" value="" size="50" class="input"  ondblclick="image_priview(this.value);"/>\
              		<input type="button" class="button" onclick="javascript:flashupload(\'image_images\', \'附件上传\',\'pics'+index+'\',submit_images,\'<?php echo (CONTROLLER_NAME); ?>\',1,1,\'<?php echo (C("cfg_image")); ?>\')" value="上传图片" /><span class="gray"> 双击可以查看图片！</span> <input type="text" name="psort[]"  value="100" class="input" style="width:50px"></td>\
			</tr>';
        obj.parent().append(html);
	})
	$('.min_pics').live('click',function(){
		$(this).parents('tr').eq(0).remove();
	})

})


</script>
</body>
</html>