<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/dwz/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
  <div class="nav">
    <ul class="cc">
      <li class="current"><a href="javascript:;">插件列表</a></li>
        
    </ul>
  </div>
  <div class="table_list">
  <table width="100%" cellspacing="0" >
    <thead>
      <tr>
        <td  width="100"  align="left">插件名称</td>
        <td  >插件说明</td>
        
        <td  width="120" align="left">作者</td>
        <td  width="100" align="left">版本</td>
        <td  width="120" align="center">上传时间</td>
        <td  width="250" align="center">管理操作</td>
      </tr>
    </thead>
    <tbody>
    <?php if($data): if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr>
    	
        <td align='left'><?php echo ($value["title"]); ?> </td>
        <td align='left'><?php echo ($value["description"]); ?> </td>
     
        <td align='left'><?php echo ($value["author"]); ?></td>
        <td align='left'><?php echo ($value["version"]); ?></td>
        <td align='center'><?php echo (format_date($value["filemtime"])); ?></td>
        <td align='center'>
          <a href="<?php echo U('info',array('remark'=>$value["name"]));?>">调用说明</a> |
          <?php if($value["install"]): ?><a href="<?php echo U('uninstall',array('remark'=>$value["name"]));?>">卸载</a>
          <?php else: ?>
            <a href="<?php echo U('info',array('remark'=>$value["name"]));?>">安装</a><?php endif; ?>
        </td>
      </tr><?php endforeach; endif; else: echo "" ;endif; ?>
      <?php else: ?>
      <tr>
        <td colspan="6">没有找到符合条件的记录</td>
      </tr><?php endif; ?>
     </tbody>
  </table>
  </div>
</div>
<script type="text/javascript" src="/dwz/Core/Org/common.js"></script>

</body>
</html>