<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dwz/Core/Org/css/admin_default_color.css" />
<?php else: ?>
<link href="/dwz/Core/Org/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
	<div class="nav">
		<ul class="cc">
			
			<li class="current">
				<a href="javascript:;">更新缓存</a>
			</li>
		</ul>
	</div>

	
		<form action="<?php echo U('cache');?>" method="post" >
		    <div class="table_full">
			<div class="h_a">更新缓存</div>
			<table width='100%'  class="table_form">
				<tr>
					<th width="200" style='vertical-align:middle'>选择更新</th>
					<td>
						<table>
							<tbody>
								<tr>
									<td>
										<label>
											<input type="checkbox" name="action[]" value="Config" checked='checked'/>
											更新网站配置
										</label>
									</td>
								</tr>

								<tr>
									<td>
										<label>
											<input type="checkbox" name="action[]" value="Category" checked='checked'/>
											栏目缓存
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" name="action[]" value="Table" checked='checked'/>
											数据表缓存
										</label>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
			
		</div>

			<div class="">
				<div class="btn_wrap_pd">
					<button class="btn btn_submit mr10 " type="submit">更新缓存</button>

				</div>
			</div>
		</form>
		



</div>
<script type="text/javascript" src="/dwz/Core/Org/common.js"></script>

</body>
</html>