<?php
/**[文档试图模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-28 19:46:37
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:48:41
 */
namespace Common\Model;
use Think\Model\ViewModel;
class ArticleAttrViewModel extends ViewModel{

	public $tableName ='article_attr';
	public $viewFields  = array(
		'article_attr'=>array(
			'*',
			'_type'=>'INNER',
		),
		'type'=>array(
			
			'_type'=>'INNER',
			'_on'=>'article_attr.type_typeid=type.typeid',
		),
		'attr'=>array(
			'*',
			'_type'=>'INNER',
			'_on'=>'article_attr.attr_attr_id=attr.attr_id',
		),
		'category'=>array(
			'_type'=>'INNER',
			'_on'=>'category.type_typeid=type.typeid',
		),
	); 

}