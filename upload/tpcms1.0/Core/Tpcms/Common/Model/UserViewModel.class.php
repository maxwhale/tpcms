<?php
/** [会员视图模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 20:52:39
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:48:41
 */
namespace Common\Model;
use Think\Model\ViewModel;
class UserViewModel extends ViewModel{

	public $tableName = 'user';

	public $viewFields  = array(
		'user'=>array(
			'*',
			'_type'=>'INNER',
		),
		'user_grade'=>array(
			'gname','gid',
			'_type'=>'INNER',
			'_on' =>'user.grade_gid=user_grade.gid',
		),
		
	); 
}