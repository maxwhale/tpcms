<?php
/** [广告视图模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 11:30:51
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:48:41
 */
namespace Common\Model;
use Think\Model\ViewModel;
class AdViewModel extends ViewModel{

	public $tableName = 'ad';

	public $viewFields  = array(
		'ad'=>array(
			'*',
			'_type'=>'INNER',
		),
		'user'=>array(
			'username','uid',
			'_type'=>'INNER',
			'_on' =>'user.uid=ad.user_uid',
		),
		'position'=>array(
			'psid','position_name',
			'_type'=>'INNER',
			'_on' =>'position.psid=ad.position_psid',
		)

	); 
}