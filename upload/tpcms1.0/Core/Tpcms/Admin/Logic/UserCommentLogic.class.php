<?php
/** [******]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-16 15:07:37
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:21
 */
namespace Admin\Logic;
use Think\Model;
class UserCommentLogic extends Model{


	/**
	 * [get_all 所有数据]
	 * @param  [type] $map         [description]
	 * @param  [type] $order       [description]
	 * @param  [type] $sort        [description]
	 * @param  [type] $currentPage [description]
	 * @param  [type] $listRows    [description]
	 * @return [type]              [description]
	 */
	public function get_all($map,$order,$sort,$currentPage,$listRows)
	{
		$data = D('UserCommentView')->where($map)->order($order.' '.$sort)->page($currentPage.','.$listRows)->select();
		
		return $data;
	}

	public function del($cmids)
	{
		$cmids = explode(',', $cmids);
		foreach($cmids as $cmid)
		{
			$this->delete($cmid);
		}

		return true;
	}

}