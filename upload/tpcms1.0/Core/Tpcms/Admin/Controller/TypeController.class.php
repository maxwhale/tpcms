<?php
/**[文档类型控制器]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-28 10:17:13
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:21:56
 */
namespace Admin\Controller;
class TypeController extends PublicController{


	public function index()
	{
		$type = $this->logic->get_all();
		$this->assign('type',$type);
		$this->display();
	}

	
}