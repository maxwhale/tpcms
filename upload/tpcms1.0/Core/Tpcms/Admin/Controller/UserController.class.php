<?php
/** [会员控制器]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 20:49:24
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:22:06
 */
namespace Admin\Controller;
class UserController extends PublicController
{
	public function _initialize()
	{
		parent::_initialize();
		// 管理员
		if(I('get.role') == 1)
		{
			$authGroup = D('AuthGroup','Logic')->get_all();
			$this->assign('authGroup',$authGroup);
			$this->assign('name','管理员');
		}

		// 会员
		if(I('get.role') == 2)
		{
			$grade = D('UserGrade','Logic')->get_all();
			$this->assign('grade',$grade);
			$this->assign('name','会员');
		}
		
	}

	/**
	 * [info 修改会员信息]
	 * @return [type] [description]
	 */
	public function info()
	{

		if(IS_POST)
		{
			if(!$this->logic->update_cur())
				$this->error($this->logic->getError());
			$this->success('修改成功',U('User/info'));
			die;
		}
		$data = $this->logic->get_one(session('uid'));
		$this->assign('data',$data);
		$this->display();	
	}

	/**
	 * [change 修改会员密码]
	 * @return [type] [description]
	 */
	public function change()
	{

		if(IS_POST)
		{
			if(!$this->logic->update_change())
				$this->error($this->logic->getError());
			$this->success('修改密码成功',U('User/change'));
			die;
		}
		
		$this->display();	
	}
}