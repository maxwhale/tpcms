<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>TPCMS内容管理系统</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Index'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Index/index';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
 	<script type='javascript' src='/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js'></script>
	<link type="text/css" rel="stylesheet" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css"></head>
	<link type="text/css" rel="stylesheet" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.index.css">
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.index.js"></script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script>
</head>
<body>	
	<div id="top-menu">
		<div class="t-l-menu">
			<?php if(is_array($topMenu)): foreach($topMenu as $key=>$top): ?><a href="javascript:" onclick="topMenu(this,<?php echo ($key); ?>)" class="active"><?php echo ($top["menu"]); ?></a><?php endforeach; endif; ?>
		</div>
		<div class="t-r-menu">
			管理员: <?php echo (session('user_name')); ?>
			<a href="javascript:;" onclick="if(confirm('确定退出')) location.href='<?php echo U("Login/out");?>'" target="_self">[退出]</a>
			<span>|</span>
			<a href="javascript:;"   onclick="runAction(this,'<?php echo U('Cache/cache');?>',999)">更新全站缓存</a>
			<span>|</span>
			<a href="/dcms/" target="_blank">前台首页</a>
		</div>
	</div>
	<!--内容区Start-->
	<div class="main">
		<!--左侧菜单Start-->
		<div id="leftMenu">
			<?php if(is_array($menu)): foreach($menu as $key=>$first): ?><div id="<?php echo ($key); ?>" class="leftMenuBlock" style="display: block;">
				<?php if(is_array($first)): foreach($first as $key=>$second): ?><dl>
					<dt><?php echo ($second["menu"]); ?></dt>
					<?php if(is_array($second["child"])): foreach($second["child"] as $key=>$third): ?><dd>
						<a href="javascript:;"  onclick="runAction(this,'<?php echo ($third["url"]); ?>',<?php echo ($third["id"]); ?>)" class="active" id='menu<?php echo ($third["id"]); ?>'><?php echo ($third["menu"]); ?></a>
					</dd><?php endforeach; endif; ?>
				</dl><?php endforeach; endif; ?>
			</div><?php endforeach; endif; ?>
		</div>
		<!--左侧菜单End-->
		<!--右侧区域Start-->
		<div id="content">
			<!--快速导航Start-->
			<div id="historyMenu">
				<div id="leftBtn">向左按钮</div>
				<div id="historyMenuBox">
					<div id="historyMenuList" style="left: 0px; width: 161px;">
						<ul>
						

						</ul>
					</div>
				</div>
				<div id="rightBtn">向右按钮</div>
			</div>
			<!--快速导航End-->
			<div class="show">
				<div class="options"> <a href="" class="refresh" id="J_refresh" title="刷新">刷新</a> <a href="" id="J_fullScreen" class="full_screen" title="全屏">全屏</a> </div>
				
			</div>
		</div>
		<!--右侧区域End-->
	</div>
	<!--内容区End-->
	<!--底部快速导航Start-->
	<!-- <div id="quickMenu">
		<div class="set">
			<a href="http://localhost/hdphp/hdcms/index.php?m=Admin&c=FavoriteMenu&a=set" target="frame">设置</a>
		</div>
		<div class="line"></div>

		<div class="menu-list"></div>
	</div> -->
	<!--底部快速导航End-->

</body>
</html>