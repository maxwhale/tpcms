<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>文档列表</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Article'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Article/index';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.article.js"></script>
	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" /><link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.article.css" />
</head>
<body>

	<script type="text/javascript" src="http://localhost/hdphp/hdcms/Static/cal/lhgcalendar.min.js"></script>
	<form class="hd-form" method="get">
	
		
		<div class="search">
			添加时间：
			<input id="begin_time" placeholder="开始时间" readonly="readonly" class="hd-w80" type="text" value="<?php echo ($_GET['search_begin_time']); ?>" name="search_begin_time">
			<script>
				$('#begin_time').calendar({
					format : 'yyyy-MM-dd'
				});
			</script>
			-
			<input id="end_time" placeholder="结束时间" readonly="readonly" class="hd-w80" type="text" value="<?php echo ($_GET['search_end_time']); ?>" name="search_end_time">
			<script>
				$('#end_time').calendar({
					format : 'yyyy-MM-dd'
				});
			</script>
			&nbsp;&nbsp;&nbsp;

			<select name="flag" class="hd-w100">
				<option  value="">全部</option>
				<?php if(is_array(C("flag"))): foreach(C("flag") as $key=>$v): ?><option value="<?php echo ($v); ?>" <?php if($_GET["flag"] == $v): ?>selected='selected'<?php endif; ?>><?php echo ($v); ?></option><?php endforeach; endif; ?>
			
			</select>
			&nbsp;&nbsp;&nbsp;
			<select name="keytype" class="hd-w100">
				
				<option value="article_title" <?php if($_GET["keytype"] == "article_title"): ?>selected='selected'<?php endif; ?>>标题</option>
				<option value="username" <?php if($_GET["keytype"] == "username"): ?>selected='selected'<?php endif; ?>>用户名</option>
			</select>
			&nbsp;&nbsp;&nbsp;
					关键字：
			<input class="hd-w200" type="text" placeholder="请输入关键字..." value="<?php echo ($_GET['keyword']); ?>" name="keyword">
			<button class="hd-btn hd-btn-xm" type="submit">搜索</button>

			<input type="hidden" name="cid" value="<?php echo ($_GET['cid']); ?>"/>
		    <input type="hidden" name="verifystate" value="<?php echo ($_GET['verifystate']); ?>"/>
		</div>
	</form>
	<div class="hd-menu-list">
		<ul>
			<li <?php if($_GET["verifystate"] == 2): ?>class="active"<?php endif; ?>>
				<a href="<?php echo U('Article/index',array('category_cid'=>$_GET['category_cid'],'verifystate'=>2));?>">内容列表</a>
			</li>
			<li <?php if($_GET["verifystate"] == 1): ?>class="active"<?php endif; ?>>
				<a href="<?php echo U('Article/index',array('category_cid'=>$_GET['category_cid'],'verifystate'=>1));?>">未审核</a>
			</li>
			<li>
				<a href="<?php echo U('Article/add',array('category_cid'=>$_GET['category_cid'],'verifystate'=>2));?>" >添加内容</a>
			</li>
		</ul>
	</div>
	<div class="hd-title-header">温馨提示</div>
	<div class="help">
	    <ul>
	        <li>
	          <?php echo ($curCname); ?> 文档列表
	        </li>
	    </ul>
	</div>
	<form action = '<?php echo U("Article/beachdelete");?>' method='post' name="operationForm">

		<table class="hd-table hd-table-list hd-form">
			<thead>
				<tr>
					<td class="hd-w30">
						<input type="checkbox" id="selectAllContent"/>
					</td>
					<td class="hd-w30">排序</td>
					<td class="hd-w30">aid</td>
					<td class="hd-w30">cid</td>
					<td>标题</td>
					<td class="hd-w50">状态</td>
					<td class="hd-w100">作者</td>
					<td class="hd-w80">添加时间</td>
					<td class="hd-w80">修改时间</td>
					<td class="hd-w120">操作</td>
				</tr>
			</thead>
			<?php if($data): if(is_array($data)): foreach($data as $key=>$v): ?><tr>
						<td class="hd-w30">
							<input type="checkbox"  name="aid[<?php echo ($v["aid"]); ?>]" value="<?php echo ($v["aid"]); ?>" />
						</td>
						<td class="hd-w30"><input type='text' class='hd-w30' name="sort[<?php echo ($v["aid"]); ?>]" value="<?php echo ($v["sort"]); ?>"></td>
						<td class="hd-w30"><?php echo ($v["aid"]); ?></td>
						<td class="hd-w30"><?php echo ($v["category_cid"]); ?></td>
						<td><?php echo ($v["article_title"]); ?> <?php if($v["flag"]): ?>[<span class='star'><?php echo ($v["flag"]); ?></span>]<?php endif; ?></td>
						<td class="hd-w50"><?php echo (set_verifystate($v["verifystate"])); ?></td>
						<td class="hd-w100"><?php echo ($v["username"]); ?></td>
						<td class="hd-w80"><?php echo (date('Y-m-d',$v["addtime"])); ?></td>
						<td class="hd-w80"><?php echo (date('Y-m-d',$v["edittime"])); ?></td>
						<td class="hd-w120">
							<a href="<?php echo U('/'.strtolower($v['remark']).'_v_'.$v['category_cid'].'_'.$v['aid']);?>" target="_blank">访问</a>
							|
							<a href="<?php echo U('Article/edit',array('category_cid'=>$v['category_cid'],'aid'=>$v['aid'],'verifystate'=>$v['verifystate']));?>">编辑</a>
							|
							<a href="javascript:;" onclick="del_modal('<?php echo U('Article/del',array('category_cid'=>$v['category_cid'],'aid'=>$v['aid'],'verifystate'=>$v['verifystate']));?>')">删除</a>
						</td>
					</tr><?php endforeach; endif; ?>
				<tr>
				<td colspan="10" class='page'><?php echo ($page); ?></td>
				</tr>
			<?php else: ?>
			<tr>
				<td colspan="10">没有找到符合的记录</td>
			</tr><?php endif; ?>

		</table>
		<div class="hd-page"></div>
		<input type="hidden" name="category_cid" value="<?php echo ($_GET['category_cid']); ?>"/>
		<input type="hidden" name="verifystate" value="<?php echo ($_GET['verifystate']); ?>"/>

		<input type="button" class="hd-btn hd-btn-xm select_all"  value="全选" />
		<input type="button" class="hd-btn hd-btn-xm operation"  value="更改排序" name="update_sort"/>

		<input type="button" class="hd-btn hd-btn-xm operation"  value="批量删除" name="update_del"/>

		<input type="button" class="hd-btn hd-btn-xm operation"  value="审核" name="update_check_state"/>
		<input type="button" class="hd-btn hd-btn-xm operation"  value="取消审核" name="update_cancle_state"/>


		<input type="button"  class="hd-btn hd-btn-xm operation" value="设置操作" name="update_check_operation" >
		<input type="button"  class="hd-btn hd-btn-xm operation" value="取消操作" name="update_cancle_operation" >
		<select name="opa" class='hd-w100'>
			<option value="0">请选择</option>
			<?php if(is_array(C("flag"))): foreach(C("flag") as $key=>$v): ?><option value="<?php echo ($v); ?>"><?php echo ($v); ?></option><?php endforeach; endif; ?>
		</select>

	</form>
	
</body>
</html>